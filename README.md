## Projet mutualisé inter SRISES `Atlas`

### Le projet
> Il s'agit d'une maqu"ette du projet Atlas

Cette maquette servira à réaliser un POC "appli shiny" de la commande SRISE d'Atlas
- vérifier rapidement la réalisabilité d'une solution applicative
  - possibilité d'afficher des cartes, de les paramétrer
  - possibilité de télécharger les cartes
  - faisabilité relativements aux données (embarquées pour l'instant dans l'application)
- tester la déployabilité de la solution
  - sur Cerise
  - sur shinyApps.io, avec le compte SSP  

### Récupérer le code

1. Git pull la branche `poc`
```
git add remote origin https://gitlab.com/srise/atlas.git
git pull origin poc
```
 
2. Lancer
   2.1 Ouvrir le fichier run/lanceur_shiny.R
   2.2 Exécuter `golem::run_dev()`

3. Ajouts
   3.1 Le fonctionnement du bouton "Dowload" (de la carte) nécessite pour l'instant
   de récupérer du JS. Télécharger phantomjs en faisant  `webshot::install_phantomjs`  dans une fenêtre R de Rstudio.

